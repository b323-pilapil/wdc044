package com.zuitt.wdc044.controller;

import com.zuitt.wdc044.exceptions.UserException;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken,@RequestBody Post post) {
        postService.createPost(stringToken,post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{post_id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long post_id, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(post_id, stringToken, post);
    }

    @RequestMapping(value="/posts/{post_id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long post_id, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(post_id, stringToken);
    }

    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getMyPosts(stringToken),HttpStatus.OK);
    }
}
