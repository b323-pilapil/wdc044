package com.zuitt.wdc044.models;

import javax.persistence.*;

// Mark this Java object as a representation of a database table via @Entity
@Entity
// Designate table name via @Table
@Table(name = "posts")

public class Post {
    // Indicates that this property represents the primary key via @Id
    @Id
    // Value for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    // Class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String content;
    @Column
    private String title;
    // Default constructor

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Post() {

    }

    // Actual constructor
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    // Getter and Setter
    // Title
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    // Content
    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}