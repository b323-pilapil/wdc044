package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}
	@GetMapping("/hello")
	public String hello(@RequestParam(value="name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}
	//@RequestMapping()

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "user") String name){
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameAge")
	public String hi(@RequestParam(value="name", defaultValue = "user") String name,@RequestParam(value="age", defaultValue = "0") int age){
		return String.format("Hello %s! Your age is %d", name,age);
	}
}
