package com.zuitt.wdc044.exceptions;

import com.zuitt.wdc044.models.User;
public class UserException  extends Exception{
    public UserException(String message){
        super(message);
    }
}
